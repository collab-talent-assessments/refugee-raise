# Refugee Crowd Funding - Skill Assessment Challenge

> Part of our #challengesforgood initiative. The work you submit will be be shared with various organizations who actively work with refugees.

**DO NOT MAKE YOUR WORK PUBLIC!** Chat me with your submission.

# The objective is to create a fundraising profile page for refugees.

The data can be obtained at the following endpoint, GraphiQL is turned on for the endpoint:

https://us-central1-refugee-d0208.cloudfunctions.net/endpoint/graphql

Example query:

```graphql
query {
  refugee(id: "anyidwilldo") {
    id
    first_name
    last_name
    # Feel free to use any available fields
  }
}
```

The page should have the following components:

* The page should display a basic profile for a refugee
* Have a button to fund the refugee, button should trigger a pop up to enter the amount to donate.
* The donation amount should update accordingly.
