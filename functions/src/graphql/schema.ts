import {} from 'node'
import * as graphqlMod from 'graphql'
import { addMockFunctionsToSchema, MockList } from 'graphql-tools'
import * as casual from 'casual'
import * as faker from 'faker'
import * as shortid from 'shortid'

const {
  GraphQLList,
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLInputObjectType,
  GraphQLID,
  GraphQLBoolean,
  graphql
} = graphqlMod

const Refugee = new GraphQLObjectType({
  name: 'Refugee',
  fields: {
    first_name: { type: GraphQLString },
    last_name: { type: GraphQLString },
    image: { type: GraphQLString },
    bio: { type: GraphQLString },
    origin_country: { type: GraphQLString },
    goals: { type: GraphQLString },
    profile_color: { type: GraphQLString },
    email: { type: GraphQLString },
    id: { type: GraphQLID },
    username: { type: GraphQLString },
    phone: { type: GraphQLString },
    amount_raised: { type: GraphQLInt }
  }
})

const query = new GraphQLObjectType({
  name: 'Query',
  fields: {
    refugee: {
      type: Refugee,
      args: {
        id: { type: GraphQLID }
      },
      resolve: (_, { id }) => {
        return { id }
      }
    },
    refugees: {
      type: new GraphQLList(Refugee)
    }
  }
})

const schema = new GraphQLSchema({
  query
})

const mocks = {
  Query: () => ({ refugees: () => new MockList(20) }),
  Refugee: () => {
    const email = casual.email

    return {
      first_name: casual.first_name,
      last_name: casual.last_name,
      image: `https://api.adorable.io/avatars/285/${email}.png`,
      bio: casual.sentences(3),
      origin_country: casual.country,
      goals: casual.sentences(3),
      profile_color: casual.rgb_hex,
      email: casual.email,
      username: casual.username,
      id: shortid.generate(),
      phone: casual.phone,
      amount_raised: casual.integer(20, 1000)
    }
  }
}

addMockFunctionsToSchema({ schema, mocks, preserveResolvers: true })

export { schema }
