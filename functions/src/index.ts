import * as functions from 'firebase-functions'
import * as express from 'express'
import { request } from 'http'
import { schema } from './graphql/schema'
import * as graphqlHTTP from 'express-graphql'
import * as cors from 'cors'

const app = express()

app.use(cors())

app.use(
  graphqlHTTP({
    schema,
    graphiql: true
  })
)

export const endpoint = functions.https.onRequest(app)
