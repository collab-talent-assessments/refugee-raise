import * as React from 'react'
import ApolloClient from 'apollo-boost'
import gql from 'graphql-tag'

class Home extends React.Component {
  componentDidMount() {
    const client = new ApolloClient({
      uri: 'http://localhost:5000/refugee-d0208/us-central1/endpoint'
    })

    client
      .query({
        query: gql`
          {
            refugee {
              first_name
            }
          }
        `
      })
      .then(result => console.log(result))
  }

  render() {
    return 'dfgdfgdfg'
  }
}

export default Home
